//
//  ActivityViewController.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 10/8/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import "ActivityViewController.h"
#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface ActivityViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *activityImage;
@property (weak, nonatomic) IBOutlet UITextField *singleTextBox;
@property (weak, nonatomic) IBOutlet UITextField *lengthTextBox;
@property (weak, nonatomic) IBOutlet UITextField *widthTextBox;
@property (weak, nonatomic) IBOutlet UITextField *heightTextBox;
@property (weak, nonatomic) IBOutlet UILabel *calAnwser;
@property (weak, nonatomic) IBOutlet UILabel *calFirstAnwser;
@property NSString* calculationType;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *myView;

@end

@implementation ActivityViewController

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    self.scrollview.minimumZoomScale = 1.0;
    self.scrollview.maximumZoomScale = 6.0;
    self.scrollview.contentSize = self.myView.frame.size;
    self.scrollview.delegate = self;

    UITapGestureRecognizer *sleepTimerTouchRecognizer = [[UITapGestureRecognizer alloc]
                                                         initWithTarget:self action:@selector(triggerSleepTimer:)];
    sleepTimerTouchRecognizer.delegate = self;
    sleepTimerTouchRecognizer.numberOfTapsRequired = 1;
    [self.scrollview addGestureRecognizer:sleepTimerTouchRecognizer];

    
    // Do any additional setup after loading the view.
    NSString *imagePath =[NSString stringWithFormat:@"%@%@", self.selectedOrigami.activityImageName, self.selectedOrigami.activityImageType];
    self.activityImage.image = [UIImage imageNamed:imagePath];
    self.calculationType = self.selectedOrigami.activityType;
        
        if([self.calculationType isEqualToString:@"Cube"] || [self.calculationType isEqualToString:@"Square"])
        {
            self.calAnwser.hidden = false;
            self.calFirstAnwser.hidden = false;
            self.singleTextBox.hidden = false;
        }
        else if([self.calculationType isEqualToString:@"Pyramid"])
        {
            self.calAnwser.hidden = false;
            self.calFirstAnwser.hidden = false;
        
            self.lengthTextBox.hidden = false;
            self.widthTextBox.hidden = false;
            self.heightTextBox.hidden = false;

        }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.myView;
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
}
-(void)viewDidAppear:(BOOL)animated{

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)triggerSleepTimer:(UITapGestureRecognizer *)recognizer
{
    [self.startMenu resetIdleTimer];

}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)editChangedSingleTextBox:(id)sender {
    UITextField *myTextField = sender;
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    
    NSString *newString = [[myTextField.text componentsSeparatedByCharactersInSet:
                            [numbersOnly invertedSet]]
                           componentsJoinedByString:@""];
    NSRange stringRange = {0, MIN([newString length], 8)};
    NSString *shortString = [newString substringWithRange:stringRange];
    if(![self checkForValidStartOfFloat:shortString])
    {
        NSRange superStrink = {0, MIN([newString length], 1)};
        shortString = [newString substringWithRange:superStrink];
    }
    myTextField.text = shortString;
    
    if(shortString.length == 0)
    {
        [self clearTextAnwsers];
    }
    else{
        [self setTextAnwsers];
        
    }
    
    
}
-(bool) checkForValidStartOfFloat :(NSString*) myString
{
    if([myString length] >0 && [myString characterAtIndex:0] == '0')
    {
        if([myString length] >1 && [myString characterAtIndex:1] != '.')
        {
            return false;
        }
    }
    return true;
}

-(void)setTextAnwsers
{
    NSString *superscript2 = @"\u00B2";
    NSString *superscript3 = @"\u00B3";
    
    if([self.calculationType isEqualToString:@"Cube"])
    {
        NSString *textBoxText = self.singleTextBox.text;

        if(textBoxText.length == 0)
        {
            return;
        }
        if(textBoxText.length == 1 && [textBoxText characterAtIndex:0] == '.')
        {
            [self clearTextAnwsers];
            return;
        }

        
        double sideLength = [textBoxText doubleValue];
        float floatAnwser = sideLength * sideLength * sideLength;

        NSString * stringlength = [self doubleToCleanString:(sideLength)];
        NSString * stringAnswer = [self doubleToCleanString:(floatAnwser)];

        self.calAnwser.text = [NSString stringWithFormat:@"%@%@%@%@", textBoxText ,superscript3, @" = ", stringAnswer];
        self.calFirstAnwser.text = [NSString stringWithFormat:@"%@", stringAnswer];
    }
    else if([self.calculationType isEqualToString:@"Square"])
    {
        NSString *textBoxText = self.singleTextBox.text;
        
        if(textBoxText.length == 0)
        {
            return;
        }
        if(textBoxText.length == 1 && [textBoxText characterAtIndex:0] == '.')
        {
            [self clearTextAnwsers];
            return;
        }
        double sideLength = [textBoxText doubleValue];
        float floatAnwser = sideLength * sideLength;
        
        NSString * stringlength = [self doubleToCleanString:(sideLength)];
        NSString * stringAnswer = [self doubleToCleanString:(floatAnwser)];

        self.calAnwser.text = [NSString stringWithFormat:@"%@%@%@%@", textBoxText ,superscript2, @" = ", stringAnswer];
        self.calFirstAnwser.text = [NSString stringWithFormat:@"%@", stringAnswer];
    }
    else if([self.calculationType isEqualToString:@"Pyramid"])
    {
        
        NSString *textBoxText = self.lengthTextBox.text;
        NSString *textBoxText2 = self.widthTextBox.text;
        NSString *textBoxText3 = self.heightTextBox.text;

        if(textBoxText.length == 0 || textBoxText2.length == 0 || textBoxText3.length == 0 )
        {
            return;
        }
        if((textBoxText.length == 1 && [textBoxText characterAtIndex:0] == '.') || (textBoxText2.length == 1 && [textBoxText2 characterAtIndex:0] == '.') || (textBoxText3.length == 1 && [textBoxText3 characterAtIndex:0] == '.'))
        {
            [self clearTextAnwsers];
            return;
        }        double length =[textBoxText doubleValue];
        double width =[textBoxText2 doubleValue];
        double height =[textBoxText3 doubleValue];

        
        double floatAnwser = length * width * height /3.0;
        
        //NSString * stringlength = [self doubleToCleanString:(length)];
        //NSString * stringwidth = [self doubleToCleanString:(width)];
        //NSString * stringheight = [self doubleToCleanString:(height)];
        NSString * stringAnswer = [self doubleToCleanString:(floatAnwser)];

        NSString* firstPartOfString = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", textBoxText, @" x ", textBoxText2, @" x ", textBoxText3, @" / 3", @" = "];
        self.calAnwser.text = [NSString stringWithFormat:@"%@%@", firstPartOfString, stringAnswer];
        self.calFirstAnwser.text = [NSString stringWithFormat:@"%@", stringAnswer];
    }
    else{
        return;
    }
    
}
-(NSString*)doubleToCleanString : (double) myDouble{
    NSString *myString = [NSString stringWithFormat:@"%f", myDouble];
    //if([myString length] != 1)
    //{
    //    myString = [myString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"0"]]];
    //}
    int length = (int)[myString length];
    for (int i = (int)[myString length]; i > 0; i--) {
        if  ([myString rangeOfString:@"."].location != NSNotFound) {
            NSRange prevChar = NSMakeRange(i-1, 1);
            if ([[myString substringWithRange:prevChar] isEqualToString:@"0"])
                length--;
            else
                break;
        }
    }
    myString = [myString substringToIndex:length];
    if([myString characterAtIndex:[myString length]-1] == '.')
    {
        myString= [myString substringToIndex:[myString length]-1];
    }
    return myString;
}
    
-(void)clearTextAnwsers
{
    self.calAnwser.text = @"";
    self.calFirstAnwser.text = @"";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
