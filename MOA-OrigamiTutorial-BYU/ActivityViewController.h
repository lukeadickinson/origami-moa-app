//
//  ActivityViewController.h
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 10/8/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoData.h"
#import "iCarouselViewController.h"

@interface ActivityViewController : UIViewController
@property VideoData* selectedOrigami;
@property iCarouselViewController* startMenu;

@end
