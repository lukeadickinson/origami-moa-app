//
//  VideoData.h
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/16/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoData : NSObject
@property NSString *name;
@property NSString *videoName;
@property NSString *videoType;
@property NSString *startImageName;
@property NSString *startImageType;
@property NSString *imageName;
@property NSString *imageType;
@property NSString *activityImageName;
@property NSString *activityImageType;
@property NSInteger imageCount;
@property NSArray * snapshotTimes;
@property NSString *estimatedTime;
@property NSString *difficulty;
@property NSString *activityType;

- (void)constructor:(NSString*)name :(NSString*)videoName :(NSString*)startImageName :(NSString*)imageName :(NSInteger)imageCount : (NSString*)activityImageName :(NSArray*)snapshotTimes : (NSString*)estimatedTime : (NSString*)difficulty : (NSString*)activityType ;
@end

