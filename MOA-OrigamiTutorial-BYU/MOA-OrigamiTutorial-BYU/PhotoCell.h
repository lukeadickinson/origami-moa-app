//
//  PhotoCell.h
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/10/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myHighlight;
@property (weak, nonatomic) IBOutlet UIImageView *myImage;
@property int index;
@end
