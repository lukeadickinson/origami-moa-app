//
//  VideoPageViewController.h
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/10/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#include "VideoData.h"
#import "iCarouselViewController.h"
@interface VideoPageViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
AVPlayer *avPlayer;
}
@property VideoData* selectedOrigami;
@property iCarouselViewController* startMenu;

@end
