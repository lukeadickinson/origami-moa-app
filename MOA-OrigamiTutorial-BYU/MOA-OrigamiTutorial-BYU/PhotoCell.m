//
//  PhotoCell.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/10/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import "PhotoCell.h"

@implementation PhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
