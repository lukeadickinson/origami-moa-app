//
//  main.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/4/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
