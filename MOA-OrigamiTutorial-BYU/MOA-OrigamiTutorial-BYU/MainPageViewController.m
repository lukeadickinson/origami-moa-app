//
//  MainPageViewController.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/10/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import "MainPageViewController.h"
#import "VideoPageViewController.h"
#import "VideoData.h"
@interface MainPageViewController ()

@property NSMutableArray *videoDataArray;
@end

@implementation MainPageViewController

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"blueBirdSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: @"Blue Bird"];
    }
    if([segue.identifier isEqualToString:@"purplePandaSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: @"Purple Panda"];
    }
    if([segue.identifier isEqualToString:@"orangeLionSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: @"Orange Lion"];
    }
    if([segue.identifier isEqualToString:@"redBoxSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: @"Red Box"];
    }
    if([segue.identifier isEqualToString:@"pinkFlowerSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: @"Pink Flower"];
    }
    if([segue.identifier isEqualToString:@"greenChairSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: @"Green Chair"];
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)initializeDefaultVideos
{
    self.videoDataArray = [NSMutableArray arrayWithObjects: nil];
    
    VideoData *videodata;
    NSString *name;
    NSString *videoName;
    NSString *startImageName;
    NSString *imageName;
    NSString *activityImageName;
    NSInteger imageCount;
    NSArray * snapShotTimes;
    NSString *estimatedTime;
    NSString *difficulty;
    
    videodata = [[VideoData alloc] init];
    name = @"Blue Bird";
    videoName = @"Bird25";
    startImageName = @"blue";
    imageName = @"blueOrigamiVideoPiece";
    imageCount = 9;
    activityImageName = @"MathAppInterface";
    snapShotTimes = @[@0, @22, @46, @70, @83, @93, @116, @117, @125];
    estimatedTime = @"5 Mintues";
    difficulty = @"Easy";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty ];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Purple Panda";
    videoName = @"Panda25";
    startImageName = @"purple";
    imageName = @"purpleOrigamiVideoPiece";
    imageCount = 9;
    activityImageName = @"Mathappinterface2";
    snapShotTimes = @[@0, @54, @93, @124, @142, @145, @172, @180, @184];
    estimatedTime = @"10 Mintues";
    difficulty = @"Easy";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty ];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Orange Lion";
    videoName = @"Lion25";
    startImageName = @"orange";
    imageName = @"orangeOrigamiVideoPiece";
    imageCount = 14;
    activityImageName = @"MathAppInterface";
    snapShotTimes = @[@0, @22, @60, @96, @135, @158, @178, @188, @205, @215, @228, @233, @240, @245];
    estimatedTime = @"15 Mintues";
    difficulty = @"Hard";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty ];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Red Box";
    videoName = @"Bomb25";
    startImageName = @"red";
    imageName = @"redOrigamiVideoPiece";
    imageCount = 13;
    activityImageName = @"Mathappinterface3";
    snapShotTimes = @[@0, @19, @34, @46, @72, @74, @106, @122, @138, @161, @178, @244, @251];
    estimatedTime = @"10 Mintues";
    difficulty = @"Easy";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty ];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Pink Flower";
    videoName = @"FlowerBox25";
    startImageName = @"pink";
    imageName = @"pinkOrigamiVideoPiece";
    imageCount = 21;
    activityImageName = @"Mathappinterface3";
    snapShotTimes = @[@0, @60, @104, @115, @117, @127, @135, @165, @197, @198, @218, @220, @252, @255, @286, @313, @333, @337, @354, @418, @445];
    estimatedTime = @"10 Mintues";
    difficulty = @"Moderate";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty ];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Green Chair";
    videoName = @"Chair25";
    startImageName = @"green";
    imageName = @"greenOrigamiVideoPiece";
    imageCount = 14;
    activityImageName = @"Mathappinterface3";
    snapShotTimes = @[@0, @92, @163, @204, @255, @293, @357, @374, @435, @535, @541, @559, @566, @570];
    estimatedTime = @"25 Mintues";
    difficulty = @"Hard";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty ];
    [self.videoDataArray addObject: videodata];
    
}
-(VideoData *)findVideoDataWithName:(NSString *)name
{
    VideoData *videodata;
    for (videodata in self.videoDataArray)
    {
        if(videodata.name == name)
        {
            return videodata;
        }
    }
    return NULL;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeDefaultVideos];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
