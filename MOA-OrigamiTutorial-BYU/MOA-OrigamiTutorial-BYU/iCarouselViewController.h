//
//  iCarouselViewController.h
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 10/1/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface iCarouselViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, retain) IBOutlet iCarousel *carousel;
- (void)resetIdleTimer;

@end