
//
//  ViewController.m
//  StoryboardExample
//
//  Created by Nick Lockwood on 08/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "iCarouselViewController.h"
#import "VideoPageViewController.h"
#import "VideoData.h"
#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
@interface iCarouselViewController ()
@property (weak, nonatomic) IBOutlet UIView *MOAImage;
@property (weak, nonatomic) IBOutlet UIView *CMRImage;

@property (nonatomic, retain) NSMutableArray *items;

@property NSMutableArray *videoDataArray;
@property (weak, nonatomic) IBOutlet UILabel *labelDifficulty;
@property NSString *videoChoice;
@property NSTimer *screenSaverTimer;
@end


@implementation iCarouselViewController


-(IBAction)moaLink:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://moa.byu.edu/"]];
}
-(IBAction)cmrLink:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://compliantmechanisms.byu.edu/"]];
}

@synthesize carousel;
@synthesize items;

- (void)awakeFromNib
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    self.items = [NSMutableArray array];
    for (int i = 0; i < 11; i++)
    {
        [items addObject:[NSNumber numberWithInt:i]];
    }
}

- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    carousel.delegate = nil;
    carousel.dataSource = nil;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad
{


    [super viewDidLoad];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [self initializeDefaultVideos];
    [self setDetails :carousel.currentItemIndex];
    
    //configure carousel
    //carousel.type = iCarouselTypeCustom;
    carousel.type = iCarouselTypeCoverFlow2;
    
    UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moaLink:)];
    [self.MOAImage addGestureRecognizer:singleTap];
    
    UITapGestureRecognizer *singleTap2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cmrLink:)];
    [self.CMRImage addGestureRecognizer:singleTap2];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.carousel = nil;
}


- (IBAction)unwindToStart:(UIStoryboardSegue *)unwindSegue
{
}


- (UIResponder *)nextResponder {
    //if([self isViewLoaded] && self.view.window && self.isActive){
    [self resetIdleTimer];
    //}
    return [super nextResponder];

}

-(void)resetIdleTimer {
    @try{

        if (!self.screenSaverTimer) {
            [[UIScreen mainScreen] setBrightness:1.0f];
            self.screenSaverTimer = [NSTimer scheduledTimerWithTimeInterval:600
                                                                     target:self
                                                                   selector:@selector(timerTick)
                                                                   userInfo:nil
                                                                    repeats:NO];
        }
        else {
            [self.screenSaverTimer invalidate];
            self.screenSaverTimer = nil;
            [self resetIdleTimer];
            //if (fabs([self.screenSaverTimer.fireDate timeIntervalSinceNow]) < 5-1.0) {
             //   [self.screenSaverTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:5]];
            //}
        }
    }
    @catch(NSException *ns)
    {
        
    }
}

-(void) timerTick
{
    [[UIScreen mainScreen] setBrightness:0.0f];
    self.screenSaverTimer = nil;
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    //if (view == nil)
   // {
        CGFloat imageDimensions = 400.0f;
        if(!IPAD)
        {
            imageDimensions = 200.0f;
        }
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageDimensions, imageDimensions)];
        
        VideoData* videoData = self.videoDataArray[index];
        NSString *imagePath =[NSString stringWithFormat:@"%@%@", videoData.startImageName, videoData.startImageType];
        ((UIImageView *)view).image = [UIImage imageNamed:imagePath];
        view.contentMode = UIViewContentModeScaleAspectFill;
    label.text = [[items objectAtIndex:index] stringValue];
    
    return view;
}
- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    CGFloat distance = 500.0f; //number of pixels to move the items away from camera
    CGFloat z = - fminf(1.0f, fabs(offset)) * distance;
    return CATransform3DTranslate(transform, offset * carousel.itemWidth, 0.0f, z);
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)objcarousel{

    [self setDetails :carousel.currentItemIndex];
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    
    if (index == carousel.currentItemIndex)
    {
        ///Selected centered item only
    
   VideoData* videoData = self.videoDataArray[index];
    self.videoChoice = videoData.name;

    [self performSegueWithIdentifier:@"VideoSegue" sender:nil];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"VideoSegue"]){
        VideoPageViewController *controller = (VideoPageViewController *)segue.destinationViewController;
        controller.selectedOrigami = [self findVideoDataWithName: self.videoChoice];
        controller.startMenu = self;
    }
}

-(void)setDetails :(NSUInteger)index
{
    VideoData* videoData = self.videoDataArray[index];
    NSString *difficultyBeginning = @"Difficulty : ";
    self.labelDifficulty.text = [NSString stringWithFormat:@"%@%@", difficultyBeginning, videoData.difficulty];
}
- (void)initializeDefaultVideos
{
    self.videoDataArray = [NSMutableArray arrayWithObjects: nil];
    
    VideoData *videodata;
    NSString *name;
    NSString *videoName;
    NSString *startImageName;
    NSString *startImageNameSize;
    NSString *imageName;
    NSString *activityImageName;
    NSString *activityImageNameSize;
    NSInteger imageCount;
    NSArray * snapShotTimes;
    NSString *estimatedTime;
    NSString *difficulty;
    NSString *activityType;

    startImageNameSize = @"400";
    activityImageNameSize =@"";
    if(!IPAD)
    {
        startImageNameSize = @"200";
        activityImageNameSize =@"Phone";
    }
    
    videodata = [[VideoData alloc] init];
    name = @"Butterfly";
    videoName = @"Butterfly25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Butterfly", startImageNameSize];
    imageName = @"butterflyOrigamiVideoPiece";
    imageCount = 14;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"Mathappinterface1"];
    snapShotTimes = @[@4, @31, @49, @50, @52, @68, @104, @119, @162, @163, @165, @174, @180, @199];
    estimatedTime = @"";
    difficulty = @"Beginner";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Blue Bird";
    videoName = @"Bird25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Bird", startImageNameSize];
    imageName = @"blueOrigamiVideoPiece";
    imageCount = 9;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"Mathappinterface2"];
    snapShotTimes = @[@0, @22, @46, @70, @83, @93, @116, @117, @125];
    estimatedTime = @"";
    difficulty = @"Beginner";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Purple Panda";
    videoName = @"Panda25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Panda", startImageNameSize];
    imageName = @"purpleOrigamiVideoPiece";
    imageCount = 9;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"Mathappinterface3"];
    snapShotTimes = @[@0, @54, @93, @124, @142, @145, @172, @180, @184];
    estimatedTime = @"";
    difficulty = @"Beginner";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Orange Lion";
    videoName = @"Lion25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Lion", startImageNameSize];
    imageName = @"orangeOrigamiVideoPiece";
    imageCount = 14;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"Mathappinterface1"];
    snapShotTimes = @[@0, @22, @60, @96, @135, @158, @178, @188, @205, @215, @228, @233, @240, @245];
    estimatedTime = @"";
    difficulty = @"Intermediate";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Red Box";
    videoName = @"Bomb25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Bomb", startImageNameSize];
    imageName = @"redOrigamiVideoPiece";
    imageCount = 13;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"MathWaterbombVolume"];
    snapShotTimes = @[@0, @19, @34, @46, @72, @74, @106, @122, @138, @161, @178, @244, @251];
    estimatedTime = @"";
    difficulty = @"Low Intermediate";
    activityType = @"Cube";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Pink Flower";
    videoName = @"FlowerBox25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"StarBox", startImageNameSize];
    imageName = @"pinkOrigamiVideoPiece";
    imageCount = 21;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"MathStarboxVolume"];
    snapShotTimes = @[@0, @60, @104, @115, @117, @127, @135, @165, @197, @198, @218, @220, @252, @255, @286, @313, @333, @337, @354, @418, @445];
    estimatedTime = @"";
    difficulty = @"Intermediate";
    activityType = @"Pyramid";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Green Chair";
    videoName = @"Chair25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Chair", startImageNameSize];
    imageName = @"greenOrigamiVideoPiece";
    imageCount = 14;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"MathChairRatios"];
    snapShotTimes = @[@0, @92, @163, @204, @255, @293, @357, @374, @435, @535, @541, @559, @566, @570];
    estimatedTime = @"";
    difficulty = @"Intermediate";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Chomper";
    videoName = @"Chomper25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Chomper", startImageNameSize];
    imageName = @"chomperOrigamiVideoPiece";
    imageCount = 13;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"Mathappinterface3"];
    snapShotTimes = @[@0, @22, @62, @102, @111, @128, @134, @147, @160, @188, @210, @217, @218];
    estimatedTime = @"";
    difficulty = @"Beginner";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Square Twist";
    videoName = @"SquareTwist25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Twist", startImageNameSize];
    imageName = @"squaretwistOrigamiVideoPiece";
    imageCount = 16;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"MathSquareTwistArea"];
    snapShotTimes = @[@0, @17, @40, @55, @73, @90, @105, @119, @143, @176, @212, @217, @223, @228, @230, @236];
    estimatedTime = @"";
    difficulty = @"Low Intermediate";
    activityType = @"Square";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Flasher";
    videoName = @"Flasher25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Flasher", startImageNameSize];
    imageName = @"flasherOrigamiVideoPiece";
    imageCount = 22;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"MathFlasherArea"];
    snapShotTimes = @[@0, @32, @48, @83, @98, @123, @152, @177, @188, @210, @242, @266, @278, @300, @309, @332, @354, @378, @413, @415, @416, @420];
    estimatedTime = @"";
    difficulty = @"Complex";
    activityType = @"Square";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
    videodata = [[VideoData alloc] init];
    name = @"Quizzer";
    videoName = @"Quizzer25";
    startImageName =  [NSString stringWithFormat:@"%@%@", @"Quizzer", startImageNameSize];
    imageName = @"quizzerOrigamiVideoPiece";
    imageCount = 13;
    activityImageName = [NSString stringWithFormat:@"%@%@", activityImageNameSize, @"Mathappinterface2"];
    snapShotTimes = @[@0, @39, @43, @115, @119, @190, @214, @217, @227, @237, @239, @245, @248];
    estimatedTime = @"";
    difficulty = @"Beginner";
    activityType = @"Plain";
    [videodata constructor:name :videoName :startImageName :imageName :imageCount :activityImageName :snapShotTimes :estimatedTime :difficulty : activityType];
    [self.videoDataArray addObject: videodata];
    
}
-(VideoData *)findVideoDataWithName:(NSString *)name
{
    VideoData *videodata;
    for (videodata in self.videoDataArray)
    {
        if(videodata.name == name)
        {
            return videodata;
        }
    }
    return NULL;
}

@end
