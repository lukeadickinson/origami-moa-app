//
//  VideoData.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/16/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import "VideoData.h"

@implementation VideoData

- (void)constructor:(NSString*)name :(NSString*)videoName :(NSString*)startImageName :(NSString*)imageName :(NSInteger)imageCount : (NSString*)activityImageName :(NSArray*)snapshotTimes : (NSString*)estimatedTime : (NSString*)difficulty : (NSString*)activityType

{
    self.name = name;
    self.videoName = videoName;
    self.videoType = @".mp4";
    self.startImageName = startImageName;
    self.startImageType = @".png";
    self.imageName = imageName;
    self.imageType = @".png";
    self.imageCount = imageCount;
    self.activityImageName = activityImageName;
    self.activityImageType = @".png";
    self.snapshotTimes = snapshotTimes;
    self.estimatedTime = estimatedTime;
    self.difficulty = difficulty;
    self.activityType = activityType;
}


@end
