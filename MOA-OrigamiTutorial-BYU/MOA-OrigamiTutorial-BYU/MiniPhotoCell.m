//
//  MiniPhotoCell.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 11/24/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//


#import "MiniPhotoCell.h"

@implementation MiniPhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end