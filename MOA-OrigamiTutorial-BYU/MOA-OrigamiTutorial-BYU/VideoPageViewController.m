//
//  VideoPageViewController.m
//  MOA-OrigamiTutorial-BYU
//
//  Created by CB187-Animation on 9/10/14.
//  Copyright (c) 2014 Brigham Young University. All rights reserved.
//

#import "VideoPageViewController.h"
#import "ActivityViewController.h"
#import "iCarouselViewController.h"
#define IPAD     UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#import "PhotoCell.h"
#import "MiniPhotoCell.h"

@interface VideoPageViewController ()
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIImageView *playHeadImage;

@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property BOOL playing;
@property BOOL loaded;
@property NSTimer* timer;
@property BOOL timerLocked;
@property NSMutableArray* cells;
@property int lastIndexAutoMovedTo;
@property (strong, nonatomic) IBOutlet UIView *outerview;

@end

@implementation VideoPageViewController


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //if(![self.selectedOrigami.name isEqualToString:@"Quizzer"])
    //{
        ActivityViewController *controller = (ActivityViewController *)segue.destinationViewController;
        controller.selectedOrigami = self.selectedOrigami;
        controller.startMenu = self.startMenu;

    //}
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
	//this section should be skipping the educational page
	//but it was crashing the app
    //if([self.selectedOrigami.name isEqualToString:@"Quizzer"])
    //{

    //    [self performSegueWithIdentifier:@"unwindToStartQuizzer" sender:self];
    //    return false;
    //}
    
    return true;
}
- (void)viewDidLoad
{
    self.cells = [NSMutableArray arrayWithObjects: nil];
    
    [self.myCollection setDataSource:self];
    [self.myCollection setDelegate:self];
    if(!IPAD)
    {
        UINib *cellNib = [UINib nibWithNibName:@"MiniPhotoCell" bundle:nil];
        [self.myCollection registerNib:cellNib forCellWithReuseIdentifier:@"MiniPhotoCell"];
        
    }
    else{
    UINib *cellNib = [UINib nibWithNibName:@"PhotoCell" bundle:nil];
    [self.myCollection registerNib:cellNib forCellWithReuseIdentifier:@"PhotoCell"];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   // UITapGestureRecognizer *sleepTimerTouchRecognizer = [[UITapGestureRecognizer alloc]
                                             //initWithTarget:self action:@selector(triggerSleepTimer:)];
   // sleepTimerTouchRecognizer.delegate = self;
    //sleepTimerTouchRecognizer.numberOfTapsRequired = 1;
    //[self.outerview addGestureRecognizer:sleepTimerTouchRecognizer];

    
    if(self.loaded == false){
        NSString *filepath = [[NSBundle mainBundle]pathForResource:self.selectedOrigami.videoName ofType: self.selectedOrigami.videoType];
        NSURL *fileURL = [NSURL fileURLWithPath:filepath];
        avPlayer = [AVPlayer playerWithURL:fileURL];
        
        AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        if(!IPAD)
        {
            layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        }
        
        layer.frame = CGRectMake(self.videoView.layer.bounds.origin.x, self.videoView.layer.bounds.origin.y,
                                 self.videoView.layer.bounds.size.width, self.videoView.layer.bounds.size.height);
        [self.videoView.layer addSublayer: layer];
        self.playing = true;
        self.loaded = true;
        
        // Create and initialize a tap gesture
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self action:@selector(respondToTapGesture:)];
        tapRecognizer.delegate = self;
        tapRecognizer.numberOfTapsRequired = 1;
        [self.videoView addGestureRecognizer:tapRecognizer];
        [self createTimer];
        [self resetPlayIcon];
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.selectedOrigami.imageCount;
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCell *cell;
    if(!IPAD)
    {
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"MiniPhotoCell" forIndexPath:indexPath];
    }
    else{
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    }
    NSInteger indexNumber = indexPath.item;

    NSString *imageName = self.selectedOrigami.imageName;
    NSString *imageType = self.selectedOrigami.imageType;
    NSInteger imageCount = self.selectedOrigami.imageCount;
       
    if(![imageName isEqual: @"none"])
    {
        for (int i =0; i<imageCount; i++) {
            NSString *imagePath =[NSString stringWithFormat:@"%@%d%@", imageName, indexNumber+1, imageType];
            cell.myImage.image=[UIImage imageNamed:imagePath];
        }
    }
    cell.index =indexNumber;
    [self.cells insertObject: cell atIndex: indexNumber];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselectAllCells];
    [self.myCollection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    NSInteger indexNumber = indexPath.item;
    [self playFrom:indexNumber];
    [self.startMenu resetIdleTimer];

}

-(void)playFrom:(NSInteger)indexNumber
{
    if(self.loaded == true){
        NSNumber *playBackTime = self.selectedOrigami.snapshotTimes[indexNumber];
        CMTime npt = CMTimeMakeWithSeconds([playBackTime integerValue], 60);
        [avPlayer seekToTime:npt toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        self.playing = true;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(!IPAD)
    {
    return CGSizeMake(82, 82);
    }
    return CGSizeMake(178, 178);

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)toggleVideo
{
    if(self.loaded == true){
        if(self.playing == false)
        {
           self.playHeadImage.hidden = false;
            [self shrinkPlayIcon];
            
            
            self.playing = true;
            [avPlayer pause];
        }
        else
        {
            self.playHeadImage.hidden = true;
            [self resetPlayIcon];
            self.playing = false;
            [avPlayer play];
            
        }
    }
}
-(void)shrinkPlayIcon{
    
    [CATransaction begin];
    [self.playHeadImage.layer removeAllAnimations];
    [CATransaction commit];
    
    CGAffineTransform translate;
    CGAffineTransform scale;
    
    scale = CGAffineTransformMakeScale(.6, .6);
    translate = CGAffineTransformMakeTranslation(self.videoView.frame.size.width/2 , 0- self.videoView.frame.size.height/2);

    if(!IPAD)
    {
        
        scale = CGAffineTransformMakeScale(.2, .2);
        translate  = CGAffineTransformMakeTranslation(0 , 0);
        //CGAffineTransform translateb = CGAffineTransformMakeTranslation(self.videoView.frame.size.width*3 , 0- self.videoView.frame.size.height*3);
       //translate =  CGAffineTransformConcat(translatea, translateb);


    }
    CGAffineTransform transform =  CGAffineTransformConcat(translate, scale);
    
    [UIView beginAnimations:@"MoveAndRotateAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:2.0];
    
    self.playHeadImage.transform = transform;
    
    [UIView commitAnimations];

}
-(void)resetPlayIcon
{
    
    [CATransaction begin];
    [self.playHeadImage.layer removeAllAnimations];
    [CATransaction commit];
    
    CGAffineTransform scale;
    scale = CGAffineTransformMakeScale(1, 1);
    
    if(!IPAD)
    {
        scale = CGAffineTransformMakeScale(.6, .6);
    }
    
    [UIView beginAnimations:@"MoveAndRotateAnimation" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.0];
    
    self.playHeadImage.transform = scale;
    
    [UIView commitAnimations];
    
}

- (IBAction)respondToTapGesture:(UITapGestureRecognizer *)recognizer
{
    [self toggleVideo];
}
- (IBAction)triggerSleepTimer:(UITapGestureRecognizer *)recognizer
{
    //calling the function empty will be good enough 
}
#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)createTimer {
    
    // create timer on run loop
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTicked:) userInfo:nil repeats:YES];
}
- (void)timerTicked:(NSTimer*)timer {
    
    double currentTime = CMTimeGetSeconds(avPlayer.currentTime);
    if(self.timerLocked == false){
        self.timerLocked = true;
        int i=0;
        for( i=0; i<self.selectedOrigami.snapshotTimes.count; i++)
        {
            if(i == self.selectedOrigami.snapshotTimes.count -1)
            {
                break;
            }
            else
            {
                NSNumber *snapShotTime = self.selectedOrigami.snapshotTimes[i+1];
                if([snapShotTime integerValue] > currentTime)
                {
                    break;
                }
            }
        }
        
        [self deselectAllCells];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        PhotoCell *cell = [self findMyCellByIndex:i];
        cell.highlighted =true;
        if(self.lastIndexAutoMovedTo != i){
        [self.myCollection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            self.lastIndexAutoMovedTo =i;
        }
        self.timerLocked = false;
    }
}
-(PhotoCell*) findMyCellByIndex :(int)index
{
    PhotoCell *cell;
    for(cell in self.cells)
    {
        if(cell.index == index)
            return cell;
    }
    return NULL;
}
-(void) deselectAllCells
{
    PhotoCell *cell;
    for(cell in self.cells)
    {
        cell.highlighted = false;
        cell.selected = false;
    }
}
- (void)timerStop{
    [self.timer invalidate];
}
- (IBAction)Cleanup:(id)sender {
    [self timerStop];
   
    [avPlayer pause];
    [self.videoView removeFromSuperview];
    [self setVideoView:nil];
}
@end
